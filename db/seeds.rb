# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

divisions = Division.create([{ title: 'Allsvenskan', rank: '1'}, { title: 'Superettan', rank: '2'}])
users = User.create([{ username: 'klonkr', battletag: 'klonkr#3212', won:'3', loss: '2', draw: '0', division_id: '1', points: '9'}, 
					 { username: 'guerilla', battletag: 'whatever#2212', won:'2', loss: '3', draw: '0', division_id: '1', points: '6'},
					 { username: 'granne', battletag: 'japp#3212', won:'4', loss: '1', draw: '0', division_id: '1', points: '12'},
					 { username: 'lol', battletag: 'japp#3212', won:'4', loss: '1', draw: '0', division_id: '2', points: '12'},
					 { username: 'what', battletag: 'japp#3212', won:'4', loss: '1', draw: '0', division_id: '2', points: '12'},
					 { username: 'japp', battletag: 'japp#3212', won:'4', loss: '1', draw: '0', division_id: '2', points: '12'},
					 { username: 'test', battletag: 'japp#3212', won:'4', loss: '1', draw: '0', division_id: '2', points: '12'}])