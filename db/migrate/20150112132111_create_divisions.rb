class CreateDivisions < ActiveRecord::Migration
  def change
    create_table :divisions do |t|
      t.string :title
      t.integer :rank
      t.string :div_region
      t.references :user, index: true

      t.timestamps
    end
  end
end
