class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :battletag
      t.integer :won
      t.integer :loss
      t.integer :draw
      t.integer :points
      t.references :division, index: true

      t.timestamps
    end
  end
end
