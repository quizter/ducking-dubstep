class DivisionsController < ApplicationController
	def index
		@divisions = Division.all
	end

	def show
		@division = Division.find(params[:id])
		@users = User.where(division_id: params[:id]).order(points: :desc)
	end

	def new
	end
end
