class UsersController < ApplicationController

	def index
		@allsvenskan = User.where(division_id: '1').order(points: :desc)
		@superettan = User.where(division_id: '2').order(points: :desc)
		@division = Division.all
		@users = User.all
	end

	def new
	end

	def show
		@user = User.find(params[:id])
	end

	def create
		@user = User.new(user_params)

		if @user.save
			redirect_to @user
		else
			render 'new'
		end
	end

	private

	def user_params
	params.require(:user).permit(:username, :battletag, :password, :email, :user_region)
	end
	
end
