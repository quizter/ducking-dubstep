class User < ActiveRecord::Base
	belongs_to :division

	validates_presence_of :username, :battletag
	validates :email, confirmation: true
	validates :password, confirmation: true
end
